/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01who;

import java.util.Scanner;

/**
 *
 * @author ali-vakili
 */
public class Day01Who {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
        System.out.println(" What is your name? ");
        String name = input.nextLine();
        System.out.println("How old are you?");
        int age = input.nextInt();
        System.out.println(" How tall are you, in meters? ");
        double heightMeters = input.nextDouble();
        System.out.println(" Nice to meet you " + name + ", you are "  + age + 
                "y/o" + heightMeters + "m tall");
        
        // verification
        if (name.equals("")){
            System.out.println("Error: Name must be not empty" );
            System.exit(1);
        }
        if (age <0 || age >150){
            System.out.println("Error: age must be between 0-150 ");
            System.exit(1);
        }
        if (heightMeters <0 || heightMeters > 3){
            System.out.println("Error: height must be between 0-3m ");
            System.exit(1);
        }
        if (age >= 18){
            System.out.println(" you are an adult ");
        } else{
            System.out.println(" grow up ");
        }
        System.out.println(" Done ");
    }
    
    
}
